<?php
require(__DIR__ . '/vendor/autoload.php');

use Sunra\PhpSimple\HtmlDomParser;
use Iodev\Whois\Whois;


function checkSll($domain)
{
    if (strpos($domain, 'https') !== false) {
        $g = stream_context_create(array("ssl" => array("capture_peer_cert" => true)));
        $r = fopen($domain, "rb", false, $g);
        $cert = stream_context_get_params($r);
        $certinfo = openssl_x509_parse($cert['options']['ssl']['peer_certificate']);
        fclose($r);
        return $certinfo['validTo_time_t'];

    }
    return false;
}

function checkDomain($domain)
{
    $parseDomain = explode("://", $domain);
    $query = str_replace('www.', '', $parseDomain[1]);
    $whois = Whois::create();
    $info = $whois->loadDomainInfo($query);
    return $info ? $info->getExpirationDate() : false;
}

if ($_GET['query'] && !empty($_GET['query'])) {
    $query = $_GET['query'] ;
    $html = file_get_contents('https://www.google.com/search?q=' . $query . '&oq=ipgone&aqs=chrome..69i57j0l5.2955j0j7&sourceid=chrome&ie=UTF-8');
    $dom = HtmlDomParser::str_get_html($html);
    $searchContainer = $dom->find('.kCrYT');
    $sites = [];
    foreach ($searchContainer as $div) {
        $url = $div->find('.UPmit', 0);
        if ($url) {
            $sites[] = explode(" ", $url->innertext)[0];
        }
    }
    $result = [];
    foreach ($sites as $site) {
        $result[$site] = [
            'ssl' => checkSll($site),
            'domain' => checkDomain($site),
        ];

    }
    echo json_encode($result);
}
?>